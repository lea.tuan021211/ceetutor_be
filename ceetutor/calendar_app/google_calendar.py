from google.oauth2 import service_account
from googleapiclient.discovery import build
import os

def create_google_calendar_event(start_time, end_time, description):
    key_file_path = "./ceetutor/static/google_calendar/ceetutor-06bc93f30793.json"
    scopes = ['https://www.googleapis.com/auth/calendar']

    # Tạo credentials từ tệp JSON và scopes
    credentials = service_account.Credentials.from_service_account_file(
        key_file_path, scopes=scopes
    )

    # Xác định dự án Google Calendar của bạn
    calendar_service = build('calendar', 'v3', credentials=credentials)

    event = {
        'summary': description,
        'description': description,
        'start': {
            'dateTime': start_time,
            'timeZone': 'UTC',
        },
        'end': {
            'dateTime': end_time,
            'timeZone': 'UTC',
        },
        'attendees': [
            {'email': 'lea.tua021211n@gmail.com'},
        ]
        }

    # Tạo sự kiện trên Google Calendar
    event = calendar_service.events().insert(calendarId='primary', body=event).execute()
    
    return event
