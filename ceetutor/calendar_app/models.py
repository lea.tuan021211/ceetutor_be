from django.db import models
import uuid

from tutor.models import Tutor
from user_profile.models import UserProfile
from ceetutor.settings import CALENDAR_STATUS_CHOICES
from ceetutor.helper import TimeSetup

# Create your models here.
class Calendar(models.Model, TimeSetup):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    start_time = models.DateTimeField()
    tutor = models.ForeignKey(Tutor, on_delete=models.CASCADE)
    student = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    tutor_note = models.TextField(blank=True, null=True)
    student_note = models.TextField(blank=True, null=True)
    status = models.CharField(choices=CALENDAR_STATUS_CHOICES, default='Active')
    booked_time = models.DateTimeField(blank=True, null=True, default=None)
    cancelled_time = models.DateTimeField(blank=True, null=True, default=None)