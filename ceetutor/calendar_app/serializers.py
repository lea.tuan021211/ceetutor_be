from rest_framework import serializers
from .models import Calendar
from user_profile.models import UserProfile
from tutor.models import Tutor
from datetime import datetime

class CustomDateTimeField(serializers.DateTimeField):
    """
    Custom serializer field to handle datetime format "YYYY-MM-DDTHH:MM"
    """

    def to_internal_value(self, data):
        try:
            # Parse the input string into a datetime object
            dt = datetime.strptime(data, "%Y-%m-%dT%H:%M")
            return dt
        except ValueError:
            raise serializers.ValidationError("Invalid datetime format. Use %Y-%m-%dT%H:%M.")

    def to_representation(self, value):
        # Serialize the datetime object into the desired format
        return value.strftime("%Y-%m-%dT%H:%M") if value else None


class CalendarSerializer(serializers.ModelSerializer):
    
    tutor_id = serializers.PrimaryKeyRelatedField(
        queryset=Tutor.objects.all(),
        required=False  
    )
    
    studend_id = serializers.PrimaryKeyRelatedField(
        queryset=UserProfile.objects.all(),
        required=False  
    )
    
    studend_note = serializers.CharField(required=False)
    
    status = serializers.CharField(required=False)
    start_time = CustomDateTimeField()
    booked_time = CustomDateTimeField(required=False, default=None)
    cancelled_time = CustomDateTimeField(required=False, default=None)
    
    class Meta:
        model = Calendar
        fields = '__all__'