from django.urls import path
from .views import CalendarListCreateView, CalendarBookingView, CalendarCancellationView, CalendarSoftDelleteView

urlpatterns = [
    path('', CalendarListCreateView.as_view(), name='calendar-list-create'),
    path('booking/<uuid:pk>/', CalendarBookingView.as_view(), name='booking'),
    path('cancellation/<uuid:pk>/', CalendarCancellationView.as_view(), name='cancellation'),
    path('delete/<uuid:pk>/', CalendarSoftDelleteView.as_view(), name='booking'),
]
