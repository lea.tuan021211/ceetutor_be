from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from datetime import timedelta, datetime

from .models import Calendar
from .serializers import CalendarSerializer
from .google_calendar import create_google_calendar_event

class CalendarListCreateView(generics.ListCreateAPIView):
    queryset = Calendar.objects.all()
    serializer_class = CalendarSerializer
    permission_classes = [IsAuthenticated]

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            self.perform_create(serializer)
            start_time = serializer.validated_data.get('start_time')
            end_time = start_time + timedelta(minutes=50)
            start_time_str = start_time.isoformat()
            end_time_str = end_time.isoformat()
            description = serializer.validated_data.get('tutor_note')
            # create_google_calendar_event(start_time_str, end_time_str, description)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class CalendarRetrieveUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Calendar.objects.all()
    serializer_class = CalendarSerializer
    permission_classes = [IsAuthenticated]

class CalendarBookingView(generics.UpdateAPIView):
    serializer_class = CalendarSerializer
    permission_classes = [IsAuthenticated]
    queryset = Calendar.objects.all()
    
    def partial_update(self, request, *args, **kwargs):
        instance = self.get_object()
        
        if instance.status == 'Booked':
            return Response({'msg': 'This schedule has been booked'}, status=status.HTTP_400_BAD_REQUEST)
        
        if instance.status == 'Cancelled':
            return Response({'msg': 'This schedule has been cancelled'}, status=status.HTTP_400_BAD_REQUEST)
        
        if request.user.role == "Student":
            data = {
                'student': user.id,
                'status': 'Booked',
                'booked_time': datetime.now()
            }
            serializer = self.get_serializer(instance, data=data, partial=True)
        
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
        
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
class CalendarCancellationView(generics.UpdateAPIView):
    serializer_class = CalendarSerializer
    permission_classes = [IsAuthenticated]
    queryset = Calendar.objects.all()
    
    def partial_update(self, request, *args, **kwargs):
        instance = self.get_object()
        
        if instance.status == 'Booked':
            return Response({'msg': 'This schedule has been booked'}, status=status.HTTP_400_BAD_REQUEST)
        
        if instance.status == 'Cancelled':
            return Response({'msg': 'This schedule has been cancelled'}, status=status.HTTP_400_BAD_REQUEST)
        
        if request.user.role == "Tutor" and instance.tutor.id == request.user.id:
            data = {
                'status': 'Cancelled',
                'cancelled_time': datetime.now()
            }
            serializer = self.get_serializer(instance, data=data, partial=True)
        
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
        
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
class CalendarSoftDelleteView(generics.UpdateAPIView):
    serializer_class = CalendarSerializer
    permission_classes = [IsAuthenticated]
    queryset = Calendar.objects.all()
    
    def partial_update(self, request, *args, **kwargs):
        instance = self.get_object()

        if request.user.role == "Tutor" and instance.tutor.id == request.user.id:
                    
            if instance.status == 'Booked':
                return Response({'msg': 'This schedule has been booked'}, status=status.HTTP_400_BAD_REQUEST)
            
            if instance.status == 'Cancelled':
                return Response({'msg': 'This schedule has been cancelled'}, status=status.HTTP_400_BAD_REQUEST)
        
            data = {
                'is_deleted': True,
            }
            serializer = self.get_serializer(instance, data=data, partial=True)
        
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
        elif request.user.role == "Admin":
            data = {
                'is_deleted': True,
            }
            serializer = self.get_serializer(instance, data=data, partial=True)
        
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
        
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        