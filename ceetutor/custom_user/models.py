import uuid
from django.contrib.auth.models import AbstractUser
from django.db import models

from ceetutor.settings import ROLES

class CustomUser(AbstractUser):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    role = models.CharField(max_length=10, choices=ROLES, default='Common')