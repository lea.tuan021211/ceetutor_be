from rest_framework import serializers
from allauth.account.forms import SignupForm

class SignupSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    password1 = serializers.CharField(write_only=True, required=True, style={'input_type': 'password'})
    password2 = serializers.CharField(write_only=True, required=True, style={'input_type': 'password'})

    def validate_email(self, value):
        # Kiểm tra xem email đã tồn tại hay chưa
        if SignupForm().check_email(value):
            raise serializers.ValidationError("Email already exists.")
        return value

    def validate(self, data):
        # Kiểm tra xem mật khẩu và mật khẩu xác nhận khớp nhau
        if data['password1'] != data['password2']:
            raise serializers.ValidationError("Passwords do not match.")
        return data
    
class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(required=True, style={'input_type': 'password'})
    new_password1 = serializers.CharField(required=True, style={'input_type': 'password'})
    new_password2 = serializers.CharField(required=True, style={'input_type': 'password'})
