from django.urls import path
from .views import custom_login, custom_signup

urlpatterns = [
    path('login/', custom_login, name='custom_login'),
    path('signup/', custom_signup, name='custom_signup'),
]