from django.apps import AppConfig


class FavoriteTutorConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'favorite_tutor'
