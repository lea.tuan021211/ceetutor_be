from rest_framework import serializers

from .models import FavoriteTutor
from custom_user.models import CustomUser
from tutor.models import Tutor

class FavoriteTutorSerializer(serializers.ModelSerializer):
    
    user = serializers.PrimaryKeyRelatedField(
        queryset=CustomUser.objects.all(),
        required=False  # Set the user field as not required
    )
    
    class Meta:
        model = FavoriteTutor
        fields = '__all__'

    def create(self, validated_data):
        # Access the authenticated user from the request context
        user = self.context['request'].user

        # Set the 'user' field to the authenticated user
        validated_data['user'] = user

        # Create the FavoriteTutor object with the 'user' field set
        favorite_tutor = FavoriteTutor.objects.create(**validated_data)

        return favorite_tutor