from django.urls import path
from .views import FavoriteTutorListCreateView, FavoriteTutorDeleteView

urlpatterns = [
    path('', FavoriteTutorListCreateView.as_view(), name='favorite-tutor-list-create'),
    path('<uuid:pk>/', FavoriteTutorDeleteView.as_view(), name='favorite-tutor-delete'),
]   