from django.shortcuts import render
from rest_framework import generics, permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from .models import FavoriteTutor
from .serializers import FavoriteTutorSerializer

class FavoriteTutorListCreateView(generics.ListCreateAPIView):
    permission_classes = [permissions.IsAuthenticated]  # Ensure only authenticated users can access the view
    
    serializer_class = FavoriteTutorSerializer

    def get_queryset(self):
        # Filter the queryset based on the authenticated user
        user = self.request.user
        queryset = FavoriteTutor.objects.filter(user=user)
        return queryset
        
class FavoriteTutorDeleteView(generics.DestroyAPIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = FavoriteTutorSerializer
    
    def get_queryset(self):
        # Filter the queryset based on the authenticated user
        user = self.request.user
        queryset = FavoriteTutor.objects.filter(user=user)
        return queryset
