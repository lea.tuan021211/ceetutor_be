from django.db import models
import uuid

from tutor.models import Tutor
from custom_user.models import CustomUser
from ceetutor.helper import TimeSetup

# Create your models here.
class FollowTutor(models.Model, TimeSetup):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    tutor = models.ForeignKey(Tutor, on_delete=models.CASCADE)
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)