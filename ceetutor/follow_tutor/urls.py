from django.urls import path
from .views import FollowTutorListCreateView, FollowTutorDeleteView

urlpatterns = [
    path('', FollowTutorListCreateView.as_view(), name='follow-tutor-list-create'),
    path('<uuid:pk>/', FollowTutorDeleteView.as_view(), name='follow-tutor-delete'),
]   