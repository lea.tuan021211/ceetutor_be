from django.shortcuts import render
from rest_framework import generics, permissions
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from .models import FollowTutor
from .serializers import FollowTutorSerializer


class FollowTutorListCreateView(generics.ListCreateAPIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = FollowTutorSerializer
    
    def get_queryset(self):
        # Filter the queryset based on the authenticated user
        user = self.request.user
        queryset = FollowTutor.objects.filter(user=user)
        return queryset

class FollowTutorDeleteView(generics.DestroyAPIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = FollowTutorSerializer
    
    def get_queryset(self):
        # Filter the queryset based on the authenticated user
        user = self.request.user
        queryset = FavoriteTutor.objects.filter(user=user)
        return queryset
