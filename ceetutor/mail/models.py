from django.db import models
from django.contrib.postgres.fields import ArrayField
import uuid

class Email(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    subject = models.CharField(max_length=255)
    message = models.TextField()
    recipients = ArrayField(models.CharField(max_length=255))

    def __str__(self):
        return self.subject
