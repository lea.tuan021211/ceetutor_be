from django.db import models
from django.contrib.postgres.fields import ArrayField
from timezone_field import TimeZoneField
import uuid

from ceetutor.settings import GENDER_CHOICES, STATUS_CHOICES
from ceetutor.helper import TimeSetup
from custom_user.models import CustomUser

class Tutor(models.Model, TimeSetup):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    gender = models.CharField(max_length=10, choices=GENDER_CHOICES, default='Other')
    time_zone = TimeZoneField(default='UTC')
    living_in = models.CharField(max_length=255)
    nationality = models.CharField(max_length=255)
    origin = models.CharField(max_length=255)
    race = models.CharField(max_length=50)
    teaching_experience = models.CharField(max_length=12)
    topics = ArrayField(models.CharField(max_length=50), null=True)
    certificate = models.CharField(max_length=20)
    other_certificate = models.CharField(max_length=255, blank=True)
    tutor_certificates = ArrayField(models.CharField(max_length=255), null=True)
    tutor_accent = models.CharField(max_length=50)
    tutor_video = models.FileField(upload_to='tutor_certificates/', null=True, blank=True, default=None)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default='Active')
    n_classes = models.IntegerField(default=0)
    n_students = models.IntegerField(default=0)
    teaching_style = models.CharField(max_length=255, default='', null=True, blank=True)
    about_me = models.CharField(max_length=255, default='', null=True, blank=True)
    education = models.CharField(max_length=255, default='', null=True, blank=True)
    
    def __str__(self):
        return f"{self.first_name} {self.last_name}"

class File(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    file = models.FileField(upload_to='tutor_certificates/')

    def __str__(self):
        return f"{self.file}"
