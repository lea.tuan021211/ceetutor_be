from rest_framework import serializers
from django.utils import timezone
import pytz
from .models import Tutor, File

class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = ('file',)

class TutorSerializer(serializers.ModelSerializer):
    tutor_certificate_files = serializers.ListField(child=serializers.FileField(), required=False)
    class Meta:
        model = Tutor
        fields = '__all__'

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['time_zone'] = instance.time_zone.key
        return data

    def create(self, validated_data):
        certificate_files = validated_data.pop('tutor_certificate_files', [])
        
        tutor_certificates = []
        for certificate in certificate_files:
            file = File.objects.create(file=certificate)
            tutor_certificates.append(file.file)
        tutor = Tutor.objects.create(tutor_certificates=tutor_certificates, **validated_data)
        return tutor
