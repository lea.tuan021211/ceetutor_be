from django.urls import path
from .views import TutorListCreateView, TutorRetrieveUpdateDeleteView, TutorsUpdateView

urlpatterns = [
    path('', TutorListCreateView.as_view(), name='tutor-list-create'),
    path('<uuid:pk>/', TutorRetrieveUpdateDeleteView.as_view(), name='tutor-retrieve-update-delete'),
    path('delete/', TutorsUpdateView.as_view(), name='delete-tutors'),
]   