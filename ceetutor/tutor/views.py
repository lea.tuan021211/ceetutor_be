from rest_framework import generics
from .models import Tutor
from .serializers import TutorSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

class TutorListCreateView(generics.ListCreateAPIView):
    authentication_classes = []
    permission_classes = []
    
    queryset = Tutor.objects.all()
    serializer_class = TutorSerializer

class TutorRetrieveUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    authentication_classes = []
    permission_classes = []
    
    queryset = Tutor.objects.all()
    serializer_class = TutorSerializer

class TutorsUpdateView(APIView):
    authentication_classes = []
    permission_classes = []

    def patch(self, request, *args, **kwargs):
        tutor_ids = request.data.get('tutor_ids', [])

        if not tutor_ids:
            return Response({"error": "Provide a list of tutor_ids to delete."}, status=status.HTTP_400_BAD_REQUEST)

        tutors = Tutor.objects.filter(id__in=tutor_ids)
        
        if not tutors:
            return Response({"error": "No tutors found with the provided tutor_ids."}, status=status.HTTP_404_NOT_FOUND)
        
        tutors.update(is_deleted=True)

        return Response({"message": f"Successfully deleted {len(tutors)} tutors."}, status=status.HTTP_200_OK)