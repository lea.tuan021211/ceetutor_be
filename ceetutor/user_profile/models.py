from django.db import models
from custom_user.models import CustomUser
from ceetutor.settings import GENDER_CHOICES, ENGLISH_LEVEL_CHOICES
import uuid

from ceetutor.helper import TimeSetup

class UserProfile(models.Model, TimeSetup):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    year_of_birth = models.IntegerField()
    gender = models.CharField(max_length=10, choices=GENDER_CHOICES)
    english_level = models.CharField(max_length=15, choices=ENGLISH_LEVEL_CHOICES)

    def __str__(self):
        return self.user.username
