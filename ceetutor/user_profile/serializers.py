from rest_framework import serializers
from .models import UserProfile
from custom_user.models import CustomUser

class UserProfileSerializer(serializers.ModelSerializer):
    
    user = serializers.PrimaryKeyRelatedField(
        queryset=CustomUser.objects.all(),
        required=False  # Set the user field as not required
    )
    class Meta:
        model = UserProfile
        fields = '__all__'

    def to_representation(self, instance):
        # Get the default serialized data
        data = super(UserProfileSerializer, self).to_representation(instance)

        # Access the authenticated user from the request context
        user = self.context['request'].user

        # Add user information to the serialized data
        data['authenticated_user'] = {
            'id': user.id,
            'username': user.username,
            'email': user.email,
            # Add any other user attributes you need
        }

        return data