from django.urls import path
from .views import UserProfileListCreateView, UserProfileRetrieveUpdateDeleteView

urlpatterns = [
    path('', UserProfileListCreateView.as_view(), name='profile-list-create'),
    path('<uuid:pk>/', UserProfileRetrieveUpdateDeleteView.as_view(), name='profile-retrieve-update-delete'),
]
