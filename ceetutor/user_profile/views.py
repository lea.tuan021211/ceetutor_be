from rest_framework import generics
from .models import UserProfile
from .serializers import UserProfileSerializer
from rest_framework.permissions import IsAuthenticated

class UserProfileListCreateView(generics.ListCreateAPIView):
    serializer_class = UserProfileSerializer

    def get_queryset(self):
        # Return only the user's own profiles
        return UserProfile.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        # Automatically associate the authenticated user with the created profile
        serializer.save(user=self.request.user)

class UserProfileRetrieveUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = UserProfileSerializer

    def get_queryset(self):
        # Return only the user's own profiles
        return UserProfile.objects.filter(user=self.request.user)
